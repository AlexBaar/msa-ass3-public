
// setup for paypal : special thanks to 
// https://www.youtube.com/watch?v=muR3W2EP3W8

const express = require('express');
const paypal = require('paypal-rest-sdk');  // step 1: we require those

paypal.configure({
  'mode': 'sandbox', //sandbox or live (live is used for production)
  'client_id': 'AeapiS94LiOP7cbywiGf2jg4a460Ba5LDBXlrxerLxEteyzb87GrXH2tsxPmtO7NVffXCNMMLdJOKOVU',
  'client_secret': 'ELPscEmJTv-gO9xux6bCb4NEftyYOTq92Bon8AYRzGadiVfhzXLNqpGGuVyrreWsGk4Bgr5gjGCj_mkE'
});

const PORT = process.env.PORT || 3000

const app = express();

app.get('/', (req, res) => res.sendFile(__dirname + "/index.html")); // sync messaging response -> <- request

app.post('/pay', (req, res) => {
    const create_payment_json = {
      "intent": "sale",
      "payer": {
          "payment_method": "paypal"
      },
      "redirect_urls": {
          "return_url": "http://localhost:3000/success",
          "cancel_url": "http://localhost:3000/cancel"
      },
      "transactions": [{
          "item_list": {
              "items": [{
                  
                  "currency": "AUD",
                  
              }]
          },
          "amount": {
              "currency": "AUD",
              "totalCost": "25.00"
          },
          "description": "Clothes for any occasion"
      }]
  };
  
  paypal.payment.create(create_payment_json, function (error, payment) {
    if (error) {
        throw error;
    } else {
        for(let i = 0;i < payment.links.length;i++){
          if(payment.links[i].rel === 'approval_url'){
            res.redirect(payment.links[i].href);
          }
        }
    }
  });
  
  });

app.listen(PORT, () => console.log(`Server Started on ${PORT}`));